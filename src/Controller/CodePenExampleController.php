<?php

namespace Drupal\code_pen_example\Controller;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\code_pen_example\Utility\DescriptionTemplateTrait;


class CodePenExampleController {
	
  use DescriptionTemplateTrait;
  use StringTranslationTrait;	


  protected function getModuleName() {
    return 'code_pen_example';
  }	
  
  
  public function getJsCodePenImplementation() {
	  
	  $title = $this->t('Code pen example');
	  
	  $build['myelement'] = [
      '#theme' => 'code_pen_example',
      '#title' => $title,
    ];

    $build['myelement']['#attached']['library'][] = 'code_pen_example/code_pen_example.script';
    // Return the renderable array.
    return $build;

	  
  }
	
}
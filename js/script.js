(function ($) {

  // Re-enable form elements that are disabled for non-ajax situations.
  Drupal.behaviors.addJqueryLines = {
    attach: function () {
      // If ajax is enabled, we want to hide items that are marked as hidden in
      // our example.
      if (Drupal.ajax) {
		      $("button").click(function(){  
				$('p').css('opacity', 0);
			  });  
      }
    }
  };
})(jQuery);